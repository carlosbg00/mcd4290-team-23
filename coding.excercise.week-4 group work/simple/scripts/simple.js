function doIt(){
    let num1Ref = document.getElementById("number1")
    let num2Ref = document.getElementById("number2")
    let num3Ref = document.getElementById("number3")
    let answerRef = document.getElementById("answer")
    let odevRef = document.getElementById("oddeven")
    
    let num1 = Number(num1Ref.value)
    let num2 = Number(num2Ref.value)
    let num3 = Number(num3Ref.value)
    let oddeven;
    let answer = num1 + num2 + num3
    answerRef.innerText = answer;
    
    
    if (answer >= 0){
        answerRef.className = "positive"
    }
    else{
        answerRef.className = "negative"
    }
    
    if (answer % 2===0){
        odevRef.innerText="(even)"
        odevRef.className="even"
    } 
    else {
        odevRef.innerText="(odd)"
        odevRef.className="odd"
    }
}
    
    
